package ru.nsu.fit.krpo.mgbackend.images;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.testcontainers.shaded.org.apache.commons.io.IOUtils;
import ru.nsu.fit.krpo.mgbackend.dao.ImageRepository;
import ru.nsu.fit.krpo.mgbackend.dao.UserRepository;
import ru.nsu.fit.krpo.mgbackend.entity.ImageEntity;
import ru.nsu.fit.krpo.mgbackend.entity.UserEntity;
import ru.nsu.fit.krpo.mgbackend.exception.ImageNotFoundException;
import ru.nsu.fit.krpo.mgbackend.service.ImageService;
import ru.nsu.fit.krpo.mgbackend.utils.ImageStatus;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ImageServiceTests {

	@Mock
	private ImageRepository imageRepository;

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private ImageService imageService;

	@Test
	public void testImageUpload() throws Exception {
		String name = "amogus";

		UserEntity user = new UserEntity();
		user.setLogin(name);
		user.setId(1L);
		when(userRepository.findByLogin(name)).thenReturn(user);

		ImageEntity entity = new ImageEntity();
		entity.setId(1L);
		when(imageRepository.save(any(ImageEntity.class))).thenReturn(entity);

		File file = new File("src/test/resources/pepesad.png");
		FileInputStream input = new FileInputStream(file);
		MultipartFile multipartFile = new MockMultipartFile(
				"file", file.getName(), "text/plain", IOUtils.toByteArray(input));


		assertDoesNotThrow(() -> imageService.uploadImage(multipartFile, name));
	}

	@Test
	public void testGetImage() {
		String name = "amogus";

		UserEntity user = new UserEntity();
		user.setLogin(name);
		user.setId(1L);

		ImageEntity entity1 = new ImageEntity();
		entity1.setId(1L);
		entity1.setOwner(1L);

		when(imageRepository.findById(1L)).thenReturn(Optional.of(entity1));

		assertDoesNotThrow(() -> imageService.downloadImage(1L));
	}

	@Test
	public void testGetImageByInvalidId() {
		String name = "amogus";

		UserEntity user = new UserEntity();
		user.setLogin(name);
		user.setId(1L);

		assertThrows(ImageNotFoundException.class, () -> imageService.downloadImage(1L));
	}

	@Test
	public void testGetImagesByAuthor() {
		String name = "amogus";
		UserEntity user = new UserEntity();
		user.setLogin(name);
		user.setId(1L);
		ImageEntity entity1 = new ImageEntity();
		entity1.setId(1L);
		entity1.setOwner(1L);
		ImageEntity entity2 = new ImageEntity();
		entity2.setId(2L);
		entity2.setOwner(1L);

		when(userRepository.existsById(1L)).thenReturn(true);
		when(imageRepository.findByOwner(1L)).thenReturn(List.of(entity1, entity2));

		var images = imageService.loadValidImageIdsByAuthor(1L);
		assertEquals(2, images.size());

		entity2.setStatus(ImageStatus.INVALID);

		images = imageService.loadValidImageIdsByAuthor(1L);
		assertEquals(1, images.size());
	}

	@Test
	public void testUpdateStatus() {
		ImageEntity entity = new ImageEntity();
		entity.setId(1L);
		entity.setOwner(1L);

		imageService.updateStatus(1L, ImageStatus.VALID);

		verify(imageRepository, times(1)).updateImageStatus(1L, ImageStatus.VALID);
	}

}
