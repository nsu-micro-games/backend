package ru.nsu.fit.krpo.mgbackend.service;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import ru.nsu.fit.krpo.mgbackend.dao.GameRepository;
import ru.nsu.fit.krpo.mgbackend.dao.ReportRepository;
import ru.nsu.fit.krpo.mgbackend.dao.UserRepository;
import ru.nsu.fit.krpo.mgbackend.dto.game.GameDto;
import ru.nsu.fit.krpo.mgbackend.entity.GameEntity;
import ru.nsu.fit.krpo.mgbackend.entity.UserEntity;
import ru.nsu.fit.krpo.mgbackend.exception.InvalidParametersException;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class GameServiceTests {
    @Mock
    private GameRepository gameRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private ReportRepository reportRepository;
    @Mock
    private TagService tagService;

    @InjectMocks
    private GameService gameService;

    private String readFileAsString(String filePath) throws IOException {
        StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
        return fileData.toString();
    }

    @BeforeEach
    public void setUp() {
        UserEntity userEntity = new UserEntity();
        String login = "ilya";
        Set<GameEntity> games = new HashSet<GameEntity>();
        userEntity.setLogin(login);
        userEntity.setGames(games);
        when(userRepository.findByLogin(login)).thenReturn(userEntity);

        GameEntity savedGame = new GameEntity();
        savedGame.setId(Long.valueOf(1));
        when(gameRepository.save(any(GameEntity.class))).thenReturn(savedGame);
    }

    @Test
    public void whenEmptyGameInCreateGame_thenSuccess() {
        GameDto gameDto = new GameDto();
        String login = "ilya";

        assertDoesNotThrow(() -> gameService.createGame(gameDto, login));
    }

    @Test
    public void createGameWithLargeDescription() {
        GameDto gameDto = new GameDto();
        String login = "ilya";
        gameDto.setDescription("aboba".repeat(200));
        assertThrows(InvalidParametersException.class, () -> gameService.createGame(gameDto, login));
    }

    @ParameterizedTest
    @ValueSource(strings = {"0", "1", "2", "3", "4"})
    public void whenValidGameInCreateGame_thenSuccess(String num) throws IOException {
        String login = "ilya";
        String name = "GGame";
        String description = "This is simple game";
        String filePath = "src/test/resources/gamejson/TestGameJson" + num + ".json";
        String gameJson = readFileAsString(filePath);

        GameDto gameDto = new GameDto();
        gameDto.setName(name);
        gameDto.setDescription(description);
        gameDto.setGameJson(gameJson);
        
        assertDoesNotThrow(() -> gameService.createGame(gameDto, login));
    }
}
