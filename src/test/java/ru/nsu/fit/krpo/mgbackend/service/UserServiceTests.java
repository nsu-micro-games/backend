package ru.nsu.fit.krpo.mgbackend.service;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import ru.nsu.fit.krpo.mgbackend.dao.UserRepository;
import ru.nsu.fit.krpo.mgbackend.entity.UserEntity;
import ru.nsu.fit.krpo.mgbackend.exception.InvalidParametersException;
import ru.nsu.fit.krpo.mgbackend.security.jwt.JwtTokenProvider;
import ru.nsu.fit.krpo.mgbackend.dto.security.RegisterDto;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class UserServiceTests {
    @Mock
    private UserRepository userRepository;
    @Mock
    private BCryptPasswordEncoder passwordEncoder;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private JwtTokenProvider jwtTokenProvider;

    @InjectMocks
    private UserService userService;

    @BeforeEach
    public void setUp() {
        UserEntity userEntity = new UserEntity();
        String login = "i1ya";
        String password = "123ilya123";
        userEntity.setLogin(login);
        userEntity.setPasswordHash(passwordEncoder.encode(password));
        when(userRepository.findByLogin(login)).thenReturn(userEntity);
    }

    @Test
    public void whenValidInRegister_thenSuccess() {
        RegisterDto registerDto = new RegisterDto();
        registerDto.setLogin("ilya");
        registerDto.setPassword("12$Ilyan123");

        assertDoesNotThrow(() -> userService.register(registerDto));
    }

    @ParameterizedTest
    @ValueSource(strings = {" ilya", "ilya ", " ilya ", "il ya"})
    public void whenLoginContainSpaceInRegister_thenTrown(String login) {
        RegisterDto registerDto = new RegisterDto();
        registerDto.setLogin(login);
        registerDto.setPassword("123ilyan123");

        assertThrows(InvalidParametersException.class, () -> userService.register(registerDto));
    }

    @ParameterizedTest
    @ValueSource(strings = {"il$ya", "ilya\n", "ilya\r", "ilya\r\n", "ilya;", "ily@"})
    public void whenLoginContainSpecialSymbolInRegister_thenTrown(String login) {
        RegisterDto registerDto = new RegisterDto();
        registerDto.setLogin(login);
        registerDto.setPassword("123ilyan123");

        assertThrows(InvalidParametersException.class, () -> userService.register(registerDto));
    }

    @ParameterizedTest
    @ValueSource(strings = {"qwerty", "12345", "qwerty12345", "1234567890", "password"})
    public void whenPasswordKnownInRedister_thenTrown() {
        RegisterDto registerDto = new RegisterDto();
        registerDto.setLogin("ilya");
        registerDto.setPassword("qwerty");

        assertThrows(InvalidParametersException.class, () -> userService.register(registerDto));
    }

    @Test
    public void whenLoginNotUniqueInRegister_thenTrown() {
        RegisterDto registerDto = new RegisterDto();
        registerDto.setLogin("i1ya");
        registerDto.setPassword("123ilyanech123");

        assertThrows(InvalidParametersException.class, () -> userService.register(registerDto));
    }

    @Test
    public void whenValidLoginAndPasswordInLogin_thenSuccess() {
        String login = "i1ya";
        String password = "123ilya123";

        assertDoesNotThrow(() -> userService.login(login, password));
    }

    @Test
    public void whenNotValidLoginInLogin_thenThrown() {
        String login = "illlya";
        String password = "123ilya123";

        assertThatThrownBy(() -> userService.login(login, password));
    }
}