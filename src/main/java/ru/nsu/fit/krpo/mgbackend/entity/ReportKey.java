package ru.nsu.fit.krpo.mgbackend.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@Embeddable
@EqualsAndHashCode
public class ReportKey implements Serializable {

    @Column(name = "user_id")
    Long userId;
    @Column(name = "game_id")
    Long gameId;
}

