package ru.nsu.fit.krpo.mgbackend.dao;

import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.fit.krpo.mgbackend.entity.Tag;

import java.util.List;
import java.util.Set;

public interface TagRepository extends JpaRepository<Tag, Long> {
    List<Tag> getTagsByNameIn(@NonNull Set<String> names);
}
