package ru.nsu.fit.krpo.mgbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MgBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MgBackendApplication.class, args);
	}
}
