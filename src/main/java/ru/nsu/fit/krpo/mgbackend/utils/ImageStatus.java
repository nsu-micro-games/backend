package ru.nsu.fit.krpo.mgbackend.utils;

/** Status of image verification. */
public enum ImageStatus {
	PENDING,
	VALID,
	INVALID
}
