package ru.nsu.fit.krpo.mgbackend.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "reported_game")
public class Report {
    @EmbeddedId
    private ReportKey id;
    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    private UserEntity user;
    @ManyToOne
    @MapsId("gameId")
    @JoinColumn(name = "game_id")
    private GameEntity game;
    private String msg;
}
