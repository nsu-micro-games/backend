package ru.nsu.fit.krpo.mgbackend.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.krpo.mgbackend.dao.GameRepository;
import ru.nsu.fit.krpo.mgbackend.dao.ReportRepository;
import ru.nsu.fit.krpo.mgbackend.dao.UserRepository;
import ru.nsu.fit.krpo.mgbackend.dto.game.GameDto;
import ru.nsu.fit.krpo.mgbackend.dto.game.GameFilterDto;
import ru.nsu.fit.krpo.mgbackend.entity.GameEntity;
import ru.nsu.fit.krpo.mgbackend.entity.Report;
import ru.nsu.fit.krpo.mgbackend.entity.ReportKey;
import ru.nsu.fit.krpo.mgbackend.entity.Tag;
import ru.nsu.fit.krpo.mgbackend.entity.UserEntity;
import ru.nsu.fit.krpo.mgbackend.exception.GameNotFoundException;
import ru.nsu.fit.krpo.mgbackend.exception.UserNotFoundException;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.nsu.fit.krpo.mgbackend.utils.Utils.validateLength;

@Service
@RequiredArgsConstructor
@Slf4j
public class GameService {
    private final GameRepository gameRepository;
    private final UserRepository userRepository;
    private final ReportRepository reportRepository;
    private final TagService tagService;

    @NonNull
    @Transactional
    public Long createGame(@NonNull GameDto gameDto, @NonNull String login) {
        validateLength(gameDto.getName(), "Name", 255);
        validateLength(gameDto.getDescription(), "Description", 255);
        final GameEntity gameEntity = new GameEntity();
        final UserEntity user = Optional.ofNullable(userRepository.findByLogin(login))
                .orElseThrow(() -> new UserNotFoundException(login));
        gameEntity.setAuthor(user);
        gameEntity.setName(gameDto.getName());
        gameEntity.setDescription(gameDto.getDescription());
        gameEntity.setGameJson(gameDto.getGameJson());
        user.addGame(gameEntity);
        final List<Tag> tags = tagService.obtainTags(gameDto.getTags());
        gameEntity.setTags(new HashSet<>(tags));
        userRepository.save(user);
        final GameEntity savedGame = gameRepository.save(gameEntity);
        return savedGame.getId();
    }

    @NonNull
    public List<GameDto> getGameList(@NonNull GameFilterDto filterDto) {
        final List<UserEntity> users = userRepository.findAllById(filterDto.getAuthorIds());
        final List<Long> gameIds = tagService.findByNames(filterDto.getTags()).stream()
                .map(Tag::getGames)
                .flatMap(Collection::stream)
                .map(GameEntity::getId)
                .distinct()
                .toList();
        final Pageable page = PageRequest.of(filterDto.getPageNumber(), filterDto.getPageSize());
        List<GameEntity> games;
        if (!users.isEmpty() && !gameIds.isEmpty()) {
            games = gameRepository.findGameEntitiesByIdInAndAuthorIn(gameIds, users, page);
        } else if (!users.isEmpty()) {
            games = gameRepository.findGameEntitiesByAuthorIn(users, page);
        } else if (!gameIds.isEmpty()) {
            games = gameRepository.findGameEntitiesByIdIn(gameIds, page);
        } else {
            games = gameRepository.findAll(page).getContent();
        }

        return games.stream()
                .map(this::mapGameToDto)
                .toList();
    }

    @NonNull
    public GameDto getGame(@NonNull Long gameId) {
        return gameRepository.findById(gameId)
                .map(this::mapGameToDto)
                .orElseThrow(() -> new GameNotFoundException(gameId));
    }

    @NonNull
    private GameDto mapGameToDto(@NonNull GameEntity game) {
        final GameDto gameDto = new GameDto();
        gameDto.setId(game.getId());
        gameDto.setAuthorId(game.getAuthor().getId());
        gameDto.setName(game.getName());
        gameDto.setDescription(game.getDescription());
        gameDto.setGameJson(game.getGameJson());
        gameDto.setTags(game.getTags().stream()
                .map(Tag::getName)
                .collect(Collectors.toSet()));
        gameDto.setUsersLikedIds(game.getLikes().stream()
                .map(UserEntity::getId)
                .collect(Collectors.toSet()));
        gameDto.setUsersDislikedIds(game.getDislikes().stream()
                .map(UserEntity::getId)
                .collect(Collectors.toSet()));
        return gameDto;
    }

    public void deleteGame(@NonNull Long gameId) {
        if (gameRepository.existsById(gameId)) {
            gameRepository.deleteById(gameId);
        }
    }

    @Transactional
    public void like(@NonNull Long gameId, @NonNull String login) {
        final GameEntity game = gameRepository.findById(gameId)
                .orElseThrow(() -> new GameNotFoundException(gameId));
        final UserEntity user = Optional.ofNullable(userRepository.findByLogin(login))
                .orElseThrow(() -> new UserNotFoundException(login));

        if (game.getLikes().contains(user)) {
            game.removeLike(user);
            user.removeLike(game);
        } else {
            game.addLike(user);
            user.addLike(game);
        }
        gameRepository.save(game);
        userRepository.save(user);
    }

    @Transactional
    public void dislike(@NonNull Long gameId, @NonNull String login) {
        final GameEntity game = gameRepository.findById(gameId)
                .orElseThrow(() -> new GameNotFoundException(gameId));
        final UserEntity user = Optional.ofNullable(userRepository.findByLogin(login))
                .orElseThrow(() -> new UserNotFoundException(login));

        if (game.getDislikes().contains(user)) {
            game.removeDislike(user);
            user.removeDislike(game);
        } else {
            game.addDislike(user);
            user.addDislike(game);
        }
        gameRepository.save(game);
        userRepository.save(user);
    }

    @Transactional
    public void report(@NonNull Long gameId, @NonNull String login, @NonNull String msg) {
        validateLength(msg, "Message", 255);
        final GameEntity game = gameRepository.findById(gameId)
                .orElseThrow(() -> new GameNotFoundException(gameId));
        final UserEntity user = Optional.ofNullable(userRepository.findByLogin(login))
                .orElseThrow(() -> new UserNotFoundException(login));
        final ReportKey key = new ReportKey();
        key.setGameId(gameId);
        key.setUserId(user.getId());
        final Report report = new Report();
        report.setId(key);
        report.setGame(game);
        report.setUser(user);
        report.setMsg(msg);
        reportRepository.save(report);
    }
}
