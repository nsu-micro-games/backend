package ru.nsu.fit.krpo.mgbackend.exception;

import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class ImageSerializationException  extends RuntimeException {
	public ImageSerializationException(@NonNull String msg, Throwable e) {
		super(msg, e);
	}
}
