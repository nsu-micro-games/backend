package ru.nsu.fit.krpo.mgbackend.dao;

import lombok.NonNull;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.fit.krpo.mgbackend.entity.GameEntity;
import ru.nsu.fit.krpo.mgbackend.entity.UserEntity;

import java.util.List;

public interface GameRepository extends JpaRepository<GameEntity, Long> {

    @NonNull
    List<GameEntity> findGameEntitiesByIdInAndAuthorIn(@NonNull List<Long> ids, @NonNull List<UserEntity> authors,
            @NonNull Pageable pageable);

    @NonNull
    List<GameEntity> findGameEntitiesByAuthorIn(@NonNull List<UserEntity> authors, @NonNull Pageable pageable);

    @NonNull
    List<GameEntity> findGameEntitiesByIdIn(@NonNull List<Long> ids, @NonNull Pageable pageable);
}
