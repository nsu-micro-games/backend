package ru.nsu.fit.krpo.mgbackend.dto.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@RequiredArgsConstructor
public class JwtDto {

	@NotNull
	String token;
}
