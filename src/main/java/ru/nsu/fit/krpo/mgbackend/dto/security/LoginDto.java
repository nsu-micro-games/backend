package ru.nsu.fit.krpo.mgbackend.dto.security;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class LoginDto {
    @NotNull
    private String login;
    @NotNull
    private String password;
}
