package ru.nsu.fit.krpo.mgbackend.dto.game;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@ToString
public class GameDto {
    private Long id;
    private Long authorId;
    @NotEmpty
    private String name;
    private String description;
    @NotEmpty
    private String gameJson;
    @NotNull
    private Set<String> tags;
    private Set<Long> usersLikedIds;
    private Set<Long> usersDislikedIds;
    private Set<Long> usersReportedIds;
}
