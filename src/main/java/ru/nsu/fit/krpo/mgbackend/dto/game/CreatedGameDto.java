package ru.nsu.fit.krpo.mgbackend.dto.game;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CreatedGameDto {
	private Long id;
}
