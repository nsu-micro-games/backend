package ru.nsu.fit.krpo.mgbackend.dto.user;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class UserFilterDto {
    @NotNull
    private String username;
    @NotNull
    private Integer pageNumber;
    @NotNull
    private Integer pageSize;
}
