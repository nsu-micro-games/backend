package ru.nsu.fit.krpo.mgbackend.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "game")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class GameEntity {
    @Id
    @EqualsAndHashCode.Include
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @EqualsAndHashCode.Include
    @JoinColumn(name = "author_id", nullable = false)
    private UserEntity author;
    @EqualsAndHashCode.Include
    private String name;
    @EqualsAndHashCode.Include
    private String description;
    @Column(columnDefinition = "text")
    @EqualsAndHashCode.Include
    private String gameJson;
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "game_tag",
            joinColumns = { @JoinColumn(name = "game_id") },
            inverseJoinColumns = { @JoinColumn(name = "tag_id") }
    )
    private Set<Tag> tags;

    @ManyToMany
    @JoinTable(
            name = "game_like",
            joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<UserEntity> likes;
    @ManyToMany
    @JoinTable(
            name = "game_dislike",
            joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<UserEntity> dislikes;

    @OneToMany(mappedBy = "game")
    private Set<Report> reports;

    public void addLike(@NonNull UserEntity user) {
        likes.add(user);
        dislikes.remove(user);
    }

    public void addDislike(@NonNull UserEntity user) {
        dislikes.add(user);
        likes.remove(user);
    }

    public void removeLike(@NonNull UserEntity user) {
        likes.remove(user);
    }

    public void removeDislike(@NonNull UserEntity user) {
        dislikes.remove(user);
    }
}
