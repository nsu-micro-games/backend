package ru.nsu.fit.krpo.mgbackend.exception;

import lombok.NonNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.nsu.fit.krpo.mgbackend.dto.ExceptionDto;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @NonNull
    @ExceptionHandler(value = {CustomNotFoundException.class})
    protected ResponseEntity<Object> handleNotFound(@NonNull RuntimeException ex, @NonNull WebRequest request) {
        final ExceptionDto exceptionDto = new ExceptionDto();
        exceptionDto.setMsg(ex.getMessage());

        return handleExceptionInternal(ex, exceptionDto, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}
