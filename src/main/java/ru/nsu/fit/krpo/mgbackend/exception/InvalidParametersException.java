package ru.nsu.fit.krpo.mgbackend.exception;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidParametersException extends RuntimeException {
	public InvalidParametersException(@NonNull String msg) {
		super(msg);
	}
}
