package ru.nsu.fit.krpo.mgbackend.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.krpo.mgbackend.dto.security.JwtDto;
import ru.nsu.fit.krpo.mgbackend.dto.security.LoginDto;
import ru.nsu.fit.krpo.mgbackend.dto.security.RegisterDto;
import ru.nsu.fit.krpo.mgbackend.dto.user.UserDto;
import ru.nsu.fit.krpo.mgbackend.dto.user.update.BioUpdateDto;
import ru.nsu.fit.krpo.mgbackend.dto.user.update.LoginUpdateDto;
import ru.nsu.fit.krpo.mgbackend.dto.user.update.PasswordUpdateDto;
import ru.nsu.fit.krpo.mgbackend.dto.user.update.PhotoUpdateDto;
import ru.nsu.fit.krpo.mgbackend.entity.UserEntity;
import ru.nsu.fit.krpo.mgbackend.service.UserService;

import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/profiles")
public class ProfilesController {
    private final UserService userService;

    @PostMapping("/register")
    @ResponseBody
    public JwtDto register(@RequestBody RegisterDto registerDto) {
        userService.register(registerDto);
        String token = userService.login(registerDto.getLogin(), registerDto.getPassword());
        JwtDto response = new JwtDto();
        response.setToken(token);
        return response;
    }

    @PostMapping("/login")
    @ResponseBody
    public JwtDto login(@RequestBody LoginDto loginDto) {
        String token = userService.login(loginDto.getLogin(), loginDto.getPassword());
        JwtDto response = new JwtDto();
        response.setToken(token);

        return response;
    }

    @PostMapping("/updatePassword")
    @ResponseBody
    public void updatePassword(@RequestBody PasswordUpdateDto password) {
        userService.updatePassword(password.getNewPassword(), SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @PostMapping("/updateLogin")
    @ResponseBody
    public JwtDto updateLogin(@RequestBody LoginUpdateDto login) {
        String token = userService.updateLogin(login.getNewLogin(), SecurityContextHolder.getContext().getAuthentication().getName());

        JwtDto response = new JwtDto();
        response.setToken(token);
        return response;
    }

    @PostMapping("/updateBio")
    @ResponseBody
    public void updateBio(@RequestBody BioUpdateDto bio) {
        userService.updateBio(bio.getNewBio(), SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @PostMapping("/updatePhoto")
    @ResponseBody
    public void updateBio(@RequestBody PhotoUpdateDto photo) {
        userService.updatePhoto(photo.getNewPhotoUrl(), SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @GetMapping("/getUser")
    public UserDto getUser() {
        var res = userService.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        UserDto user = new UserDto();
        user.setId(res.getId());
        user.setLogin(res.getLogin());
        user.setPhotoUrl(res.getPhotoUrl());
        user.setBio(res.getBio());
        return user;
    }
}
