package ru.nsu.fit.krpo.mgbackend.security;

public enum Role {
    USER,
    ADMIN
}
