package ru.nsu.fit.krpo.mgbackend.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.krpo.mgbackend.dto.user.UserDto;
import ru.nsu.fit.krpo.mgbackend.dto.user.UserFilterDto;
import ru.nsu.fit.krpo.mgbackend.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/list")
    public List<UserDto> getUserList(@RequestBody @Valid UserFilterDto userFilterDto) {
        return userService.getUserList(userFilterDto);
    }

    @GetMapping("/{userId}")
    public UserDto getUser(@PathVariable Long userId) {
        return userService.getUser(userId);
    }
}
