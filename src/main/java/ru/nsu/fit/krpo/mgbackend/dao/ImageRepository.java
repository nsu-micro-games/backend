package ru.nsu.fit.krpo.mgbackend.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.nsu.fit.krpo.mgbackend.entity.ImageEntity;
import ru.nsu.fit.krpo.mgbackend.utils.ImageStatus;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface ImageRepository extends JpaRepository<ImageEntity, Long> {

	List<ImageEntity> findByOwner(Long ownerId);

	Optional<ImageEntity> findById(Long imageId);

	@Transactional
	@Modifying
	@Query("update ImageEntity image set image.status = :status where image.id = :id")
	void updateImageStatus(@Param(value = "id") long id, @Param(value = "status")ImageStatus status);
}
