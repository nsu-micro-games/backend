package ru.nsu.fit.krpo.mgbackend.exception;

import lombok.NonNull;


public class UserNotFoundException extends CustomNotFoundException {

    public UserNotFoundException(@NonNull Long id) {
        super("User with id = " + id + " not found.");
    }

    public UserNotFoundException(@NonNull String username) {
        super("User '" + username + "' not found.");
    }
}
