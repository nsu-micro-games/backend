package ru.nsu.fit.krpo.mgbackend.dao;

import lombok.NonNull;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;
import ru.nsu.fit.krpo.mgbackend.entity.UserEntity;

import javax.transaction.Transactional;
import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Nullable
    UserEntity findByLogin(@NonNull String login);

    @NonNull
    @Query(value = "\n" +
            "SELECT u FROM UserEntity u " +
            "WHERE u.login LIKE ?1% ")
    List<UserEntity> findAllByLoginILike(@NonNull String login, @NonNull Pageable pageable);

    @Modifying
    @Transactional
    @Query("update UserEntity user set user.login = :login where user.id = :id")
    void updateLogin(@Param(value = "id") long id, @Param(value = "login") String login);

    @Modifying
    @Transactional
    @Query("update UserEntity user set user.passwordHash = :passwordHash where user.id = :id")
    void updatePassword(@Param(value = "id") long id, @Param(value = "passwordHash") String passwordHash);

    @Modifying
    @Transactional
    @Query("update UserEntity user set user.bio = :bio where user.id = :id")
    void updateBio(@Param(value = "id") long id, @Param(value = "bio") String bio);

    @Modifying
    @Transactional
    @Query("update UserEntity user set user.photoUrl = :photoUrl where user.id = :id")
    void updatePhoto(@Param(value = "id") long id, @Param(value = "photoUrl") String photoUrl);

}
