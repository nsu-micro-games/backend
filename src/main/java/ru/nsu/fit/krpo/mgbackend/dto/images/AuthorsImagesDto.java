package ru.nsu.fit.krpo.mgbackend.dto.images;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@ToString
public class AuthorsImagesDto {

	@NotNull
	List<Long> imageIds;
}
