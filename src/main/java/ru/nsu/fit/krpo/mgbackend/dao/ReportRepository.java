package ru.nsu.fit.krpo.mgbackend.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.fit.krpo.mgbackend.entity.Report;

public interface ReportRepository extends JpaRepository<Report, Long> {
}
