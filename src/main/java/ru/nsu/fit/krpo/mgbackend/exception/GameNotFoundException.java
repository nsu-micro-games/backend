package ru.nsu.fit.krpo.mgbackend.exception;

import lombok.NonNull;

public class GameNotFoundException extends CustomNotFoundException {

    public GameNotFoundException(@NonNull Long id) {
        super("Game " + id + "not found.");
    }
}
