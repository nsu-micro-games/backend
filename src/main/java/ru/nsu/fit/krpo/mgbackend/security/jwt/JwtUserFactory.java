package ru.nsu.fit.krpo.mgbackend.security.jwt;

import lombok.NonNull;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ru.nsu.fit.krpo.mgbackend.entity.UserEntity;
import ru.nsu.fit.krpo.mgbackend.security.Role;

import java.util.List;

public final class JwtUserFactory {

    private JwtUserFactory() {
        throw new NotImplementedException();
    }

    @NonNull
    public static JwtUser create(@NonNull UserEntity user) {
        return new JwtUser(
                user.getId(),
                user.getLogin(),
                user.getPasswordHash(),
                mapToGrantedAuthorities(user.getRole())
        );
    }

    @NonNull
    private static List<GrantedAuthority> mapToGrantedAuthorities(@NonNull Role userRole) {
        return List.of(new SimpleGrantedAuthority(userRole.name()));
    }
}
