package ru.nsu.fit.krpo.mgbackend.dto.images;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.Nullable;
import org.springframework.web.multipart.MultipartFile;
import ru.nsu.fit.krpo.mgbackend.utils.ImageStatus;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class ImageUploadDto {

	@NotNull
	MultipartFile image;
}
