package ru.nsu.fit.krpo.mgbackend.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.nsu.fit.krpo.mgbackend.dao.UserRepository;
import ru.nsu.fit.krpo.mgbackend.entity.UserEntity;
import ru.nsu.fit.krpo.mgbackend.exception.UserNotFoundException;
import ru.nsu.fit.krpo.mgbackend.security.jwt.JwtUser;
import ru.nsu.fit.krpo.mgbackend.security.jwt.JwtUserFactory;

@Slf4j
@Service
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final UserEntity user = userRepository.findByLogin(username);

        if (user == null ) {
            throw new UserNotFoundException(username);
        }

        final JwtUser jwtUser = JwtUserFactory.create(user);
        log.info("User with username: {} successfully loaded.", username);
        return jwtUser;
    }
}
