package ru.nsu.fit.krpo.mgbackend.utils;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.fit.krpo.mgbackend.exception.InvalidParametersException;

import java.util.Arrays;

@Slf4j
public class Utils {
	public static String log(StackTraceElement[] stackTrace) {
		return Arrays.toString(stackTrace);
	}

	public static void validateLength(String field, String name, int length) {
		if (field != null && field.length() > 255) {
			log.info(name + "'s length exceeds " + length + " characters");
			throw new InvalidParametersException(name + "'s length exceeds " + length + " characters");
		}
	}
}
