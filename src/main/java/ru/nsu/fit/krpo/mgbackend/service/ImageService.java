package ru.nsu.fit.krpo.mgbackend.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;
import ru.nsu.fit.krpo.mgbackend.dao.ImageRepository;
import ru.nsu.fit.krpo.mgbackend.dao.UserRepository;
import ru.nsu.fit.krpo.mgbackend.dto.images.VerificationDto;
import ru.nsu.fit.krpo.mgbackend.entity.ImageEntity;
import ru.nsu.fit.krpo.mgbackend.entity.UserEntity;
import ru.nsu.fit.krpo.mgbackend.exception.ImageNotFoundException;
import ru.nsu.fit.krpo.mgbackend.exception.ImageSerializationException;
import ru.nsu.fit.krpo.mgbackend.exception.UserNotFoundException;
import ru.nsu.fit.krpo.mgbackend.utils.ImageStatus;
import ru.nsu.fit.krpo.mgbackend.utils.Utils;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ImageService {

	private final ImageRepository imageRepository;
	private final UserRepository userRepository;

	private final ExecutorService executor = Executors.newCachedThreadPool();

	@Value("${moderation.address}")
	private String address;

	static final String SCORING_URL_TEMPLATE = "/scoring";

	public long uploadImage(MultipartFile imageFile, @NonNull String login) {
		final UserEntity user = Optional.ofNullable(userRepository.findByLogin(login))
				.orElseThrow(() -> new UserNotFoundException(login));
		log.info("Storing image with author id {}", user.getId());
		try {
			byte[] data = imageFile.getBytes();
			var imageToSave = ImageEntity.builder()
					.status(ImageStatus.PENDING)
					.data(imageFile.getBytes())
					.owner(user.getId())
					.build();
			var added = imageRepository.save(imageToSave);
			log.info("Stored image {}, sending to moderation service", added.getId());

			executor.submit(
					() -> {
						try
						{
							RestTemplate restTemplate = new RestTemplate();
							String baseUrl = address + SCORING_URL_TEMPLATE;

							String url = UriComponentsBuilder.fromHttpUrl(baseUrl).queryParam("id", added.getId()).encode()
									.toUriString();

							HttpHeaders headers = new HttpHeaders();
							headers.setContentType(MediaType.MULTIPART_FORM_DATA);
							MultiValueMap<String, Object> args = new LinkedMultiValueMap<>();
							args.add("file", data);
							HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(args, headers);
							log.info("Asking moderation service for moderation of " + added.getId());
							ResponseEntity<VerificationDto> response = restTemplate.postForEntity(url, request, VerificationDto.class);

							VerificationDto result = response.getBody();
							ImageStatus status = extractStatus(result);

							log.info(added.getId() + " was moderated. Result is: " + status);

							updateStatus(added.getId(), status);
							log.info("Updated status of " + added.getId() + " to " + status);
						} catch (Throwable e) {
							log.error(e.getMessage());
						}
					});
			return added.getId();
		}
		catch (IOException e) {
			log.error("Could not store image, {}", e.getMessage());
			log.error(Utils.log(e.getStackTrace()));
			throw new ImageSerializationException("Could not extract bytes from image", e);
		}
	}

	public void updateStatus(Long imageId, ImageStatus status) {
		log.info("Updating status of {} to {}", imageId, status);
		imageRepository.updateImageStatus(imageId, status);
	}

	public ImageEntity downloadImage(Long imageId) {
		return imageRepository.findById(imageId).orElseThrow(() -> new ImageNotFoundException(imageId));
	}

	public List<Long> loadValidImageIdsByAuthor(long authorId) {
		if (!userRepository.existsById(authorId)) {
			throw new UserNotFoundException(authorId);
		}
		return imageRepository.findByOwner(authorId).stream()
				.filter(x -> x.getStatus() != ImageStatus.INVALID)
				.map(ImageEntity::getId)
				.collect(Collectors.toList());
	}

	private ImageStatus extractStatus(VerificationDto verificationResult) {
		if (verificationResult.getResult() == 1) {
			return ImageStatus.VALID;
		}
		return ImageStatus.INVALID;
	}
}
