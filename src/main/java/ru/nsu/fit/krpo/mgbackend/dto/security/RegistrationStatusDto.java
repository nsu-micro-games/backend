package ru.nsu.fit.krpo.mgbackend.dto.security;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class RegistrationStatusDto {

	@NotNull
	boolean isRegistered;

	@NotNull
	String message;
}
