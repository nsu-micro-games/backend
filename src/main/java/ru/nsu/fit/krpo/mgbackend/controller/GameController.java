package ru.nsu.fit.krpo.mgbackend.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.krpo.mgbackend.dto.game.CreatedGameDto;
import ru.nsu.fit.krpo.mgbackend.dto.game.GameDto;
import ru.nsu.fit.krpo.mgbackend.dto.game.GameFilterDto;
import ru.nsu.fit.krpo.mgbackend.service.GameService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/games")
@RequiredArgsConstructor
@Log
public class GameController {

    private final GameService gameService;

    @PostMapping("/create")
    public CreatedGameDto createGame(@RequestBody @Valid GameDto gameDto) {
        long gameId = gameService.createGame(gameDto, SecurityContextHolder.getContext().getAuthentication().getName());
        CreatedGameDto newGame = new CreatedGameDto();
        newGame.setId(gameId);
        return newGame;
    }

    @PostMapping("/list")
    public List<GameDto> getGameList(@RequestBody @Valid GameFilterDto filterDto) {
        return gameService.getGameList(filterDto);
    }

    @GetMapping("/{gameId}")
    public GameDto getGame(@PathVariable Long gameId) {
        return gameService.getGame(gameId);
    }

    @PostMapping("/delete/{gameId}")
    public void deleteGame(@PathVariable Long gameId) {
        gameService.deleteGame(gameId);
    }

    @PostMapping("/like/{gameId}")
    public void likeGame(@PathVariable Long gameId) {
        gameService.like(gameId, SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @PostMapping("/dislike/{gameId}")
    public void dislikeGame(@PathVariable Long gameId) {
        gameService.dislike(gameId, SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @PostMapping("/report/{gameId}")
    public void reportGame(@PathVariable Long gameId, @RequestBody String report) {
        gameService.report(gameId, SecurityContextHolder.getContext().getAuthentication().getName(), report);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handle(HttpMessageNotReadableException e) {
        log.info("Returning HTTP 400 Bad Request" + e.getMessage());
    }
}
