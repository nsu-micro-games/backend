package ru.nsu.fit.krpo.mgbackend.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import ru.nsu.fit.krpo.mgbackend.security.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "mg_user")
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class UserEntity {
    @Id
    @ToString.Include
    @EqualsAndHashCode.Include
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ToString.Include
    @EqualsAndHashCode.Include
    @Column(unique = true)
    private String login;

    @OneToMany(mappedBy = "author")
    private Set<GameEntity> games;

    @ToString.Include
    @EqualsAndHashCode.Include
    private String passwordHash;

    private String photoUrl;

    private String bio;

    @ManyToMany(mappedBy = "likes")
    private Set<GameEntity> likes;

    @ManyToMany(mappedBy = "dislikes")
    private Set<GameEntity> dislikes;

    @OneToMany(mappedBy = "user")
    private Set<Report> reports;

    @ToString.Include
    private Role role;

    public void addGame(@NonNull GameEntity gameEntity) {
        games.add(gameEntity);
        gameEntity.setAuthor(this);
    }

    public void addLike(@NonNull GameEntity game) {
        likes.add(game);
        dislikes.remove(game);
    }

    public void addDislike(@NonNull GameEntity game) {
        dislikes.add(game);
        likes.remove(game);
    }

    public void removeLike(@NonNull GameEntity game) {
        likes.remove(game);
    }

    public void removeDislike(@NonNull GameEntity game) {
        dislikes.remove(game);
    }
}
