package ru.nsu.fit.krpo.mgbackend.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.krpo.mgbackend.dao.TagRepository;
import ru.nsu.fit.krpo.mgbackend.entity.Tag;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class TagService {
    private final TagRepository tagRepository;

    @Transactional
    public List<Tag> obtainTags(@NonNull Set<String> names) {
        final List<Tag> tags = tagRepository.getTagsByNameIn(names);
        names.stream()
                .filter(n -> !tags.stream()
                        .map(Tag::getName)
                        .toList()
                        .contains(n))
                .forEach(n -> {
                    final Tag tag = new Tag();
                    tag.setName(n);
                    tagRepository.save(tag);
                    tags.add(tag);
                });
        return tags;
    }

    public List<Tag> findByNames(@NonNull Set<String> names) {
        return tagRepository.getTagsByNameIn(names);
    }
}
