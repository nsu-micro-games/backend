package ru.nsu.fit.krpo.mgbackend.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.nsu.fit.krpo.mgbackend.dao.UserRepository;
import ru.nsu.fit.krpo.mgbackend.dto.security.RegisterDto;
import ru.nsu.fit.krpo.mgbackend.dto.user.UserDto;
import ru.nsu.fit.krpo.mgbackend.dto.user.UserFilterDto;
import ru.nsu.fit.krpo.mgbackend.entity.UserEntity;
import ru.nsu.fit.krpo.mgbackend.exception.InvalidParametersException;
import ru.nsu.fit.krpo.mgbackend.exception.UserNotFoundException;
import ru.nsu.fit.krpo.mgbackend.security.Role;
import ru.nsu.fit.krpo.mgbackend.security.jwt.JwtTokenProvider;

import java.util.List;

import static ru.nsu.fit.krpo.mgbackend.utils.Utils.validateLength;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    @NonNull
    public List<UserDto> getUserList(@NonNull UserFilterDto userFilterDto) {
        final Pageable userPage = PageRequest.of(userFilterDto.getPageNumber(), userFilterDto.getPageSize());
        return userRepository.findAllByLoginILike(userFilterDto.getUsername(), userPage).stream()
                .map(user -> {
                    final UserDto userDto = new UserDto();
                    userDto.setId(user.getId());
                    userDto.setLogin(user.getLogin());
                    userDto.setPhotoUrl(user.getPhotoUrl());
                    userDto.setBio(user.getBio());
                    return userDto;
                }).toList();
    }

    @NonNull
    public UserDto getUser(@NonNull Long userId) {
        return userRepository.findById(userId)
                .map(user -> {
                    final UserDto userDto = new UserDto();
                    userDto.setId(user.getId());
                    userDto.setLogin(user.getLogin());
                    userDto.setPhotoUrl(user.getPhotoUrl());
                    userDto.setBio(user.getBio());
                    return userDto;
                })
                .orElseThrow(() -> new UserNotFoundException(userId));
    }

    @NonNull
    public String login(@NonNull String login, @NonNull String password) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(login, password));
        final UserEntity user = userRepository.findByLogin(login);
        if (user == null) {
            throw new UserNotFoundException(login);
        }

        return jwtTokenProvider.createToken(login, user.getRole());
    }

    @NonNull
    public UserEntity getUserByLogin(@NonNull String login) {
        UserEntity result = userRepository.findByLogin(login);
        if (result == null) {
            throw new UserNotFoundException(login);
        }
        return result;
    }

    public void register(@NonNull RegisterDto registerDto) {
        validateLength(registerDto.getLogin(), "Login", 255);
        validateLength(registerDto.getPhotoUrl(), "PhotoUrl", 255);
        validateLength(registerDto.getBio(), "Bio", 255);
        if (!validateLogin(registerDto.getLogin())) {
            log.info("Login {} contains invalid symbols", registerDto.getLogin());
            throw new InvalidParametersException("Login contains invalid symbols");
        }

        if (!validatePassword(registerDto.getPassword())) {
            log.info("Password for " + registerDto.getLogin() + " is too weak");
            throw new InvalidParametersException("Password is too weak");
        }

        try {
            final UserEntity user = new UserEntity();
            user.setPasswordHash(passwordEncoder.encode(registerDto.getPassword()));
            user.setRole(Role.USER);
            user.setLogin(registerDto.getLogin());
            user.setPhotoUrl(registerDto.getPhotoUrl());
            userRepository.save(user);
            log.info("User registered {}", user);
        }
        catch (Exception e) {
            log.info("User " + registerDto.getLogin() + " already exists");
            throw new InvalidParametersException("User with that login exists");
        }
    }

    public String updateLogin(@NonNull String newLogin, String userName) {
        validateLength(newLogin, "Login", 255);
        if (!validateLogin(newLogin)) {
            log.info("Login " + newLogin + " is invalid");
            throw new InvalidParametersException("New login " + newLogin + " is invalid");
        }
        UserEntity entity = userRepository.findByLogin(userName);
        if (entity == null) {
            log.error("User with login " + userName + " doesn't exist");
            throw new InvalidParametersException("User with login " + userName + " doesn't exist");
        }
        UserEntity entity1 = userRepository.findByLogin(newLogin);
        if (entity1 != null) {
            log.info("User with login " + newLogin + " already exists");
            throw new InvalidParametersException("User with login " + newLogin + " already exists");
        }
        userRepository.updateLogin(entity.getId(), newLogin);
        return jwtTokenProvider.createToken(newLogin, entity.getRole());
    }

    public void updatePassword(@NonNull String newPassword, String userName) {
        if (!validatePassword(newPassword)) {
            log.info("New password for user " + userName + " is too weak");
            throw new InvalidParametersException("New password for user " + userName + " is too weak");
        }
        UserEntity entity = userRepository.findByLogin(userName);
        if (entity == null) {
            log.error("Could not find user with name: " + userName);
            throw new InvalidParametersException("Could not find user with name: " + userName);
        }
        userRepository.updatePassword(entity.getId(), passwordEncoder.encode(newPassword));
    }

    public void updateBio(@NonNull String newBio, String userName) {
        validateLength(newBio, "Bio", 255);
        UserEntity entity = userRepository.findByLogin(userName);
        if (entity == null)
        {
            log.error("Could not find user with name: " + userName);
            throw new InvalidParametersException("Could not find user with name: " + userName);
        }
        userRepository.updateBio(entity.getId(), newBio);
    }

    public void updatePhoto(@NonNull String newPhoto, String userName) {
        validateLength(newPhoto, "PhotoUrl", 255);
        UserEntity entity = userRepository.findByLogin(userName);
        if (entity == null) {
            log.error("Could not find user with name: " + userName);
            throw new InvalidParametersException("Could not find user with name: " + userName);
        }
        userRepository.updatePhoto(entity.getId(), newPhoto);
    }

    private boolean validateLogin(String login) {
        for (var c : login.toCharArray()) {
            if (!Character.isAlphabetic(c) && !Character.isDigit(c)) return false;
        }
        return true;
    }

    private boolean validatePassword(String password) {
        if (password.length() < 8) return false;

        boolean hasDigit = false;
        boolean hasUpper = false;
        boolean hasLower = false;
        boolean hasSpecial = false;

        String special = "!@#$%^&*";

        for (var c : password.toCharArray()) {
            if (Character.isDigit(c)) hasDigit = true;
            if (Character.isLowerCase(c)) hasLower = true;
            if (Character.isUpperCase(c)) hasUpper = true;
            if (special.indexOf(c)!= -1) hasSpecial = true;
        }

        return hasDigit && hasUpper && hasLower &&hasSpecial;
    }
}
