package ru.nsu.fit.krpo.mgbackend.exception;

import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ImageNotFoundException extends RuntimeException {
	public ImageNotFoundException(@NonNull Long id) {
		super("Image " + id + "not found.");
	}
}
