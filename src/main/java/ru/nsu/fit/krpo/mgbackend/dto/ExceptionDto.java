package ru.nsu.fit.krpo.mgbackend.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExceptionDto {
    private String msg;
}
