package ru.nsu.fit.krpo.mgbackend.dto.images;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class ImagesRequestDto {

	@NotNull
	Long imageId;
}
