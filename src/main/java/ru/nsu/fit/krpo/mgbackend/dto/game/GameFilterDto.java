package ru.nsu.fit.krpo.mgbackend.dto.game;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@ToString
public class GameFilterDto {
    @NotNull
    private Set<String> tags;
    @NotNull
    private Set<Long> authorIds;
    @NotNull
    private Integer pageSize;
    @NotNull
    private Integer pageNumber;
}
