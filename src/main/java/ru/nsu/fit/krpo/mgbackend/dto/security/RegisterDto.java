package ru.nsu.fit.krpo.mgbackend.dto.security;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class RegisterDto {
    @NotNull
    private String login;
    @NotNull
    private String password;
    private String photoUrl;
    private String bio;
}
