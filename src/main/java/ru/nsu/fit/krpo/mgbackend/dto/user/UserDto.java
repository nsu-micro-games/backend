package ru.nsu.fit.krpo.mgbackend.dto.user;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserDto {
    private long id;
    private String login;
    private String photoUrl;
    private String bio;
}
