package ru.nsu.fit.krpo.mgbackend.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.krpo.mgbackend.dto.images.AuthorsImagesDto;
import ru.nsu.fit.krpo.mgbackend.dto.images.ImageCreatedDto;
import ru.nsu.fit.krpo.mgbackend.dto.images.ImageUploadDto;
import ru.nsu.fit.krpo.mgbackend.entity.ImageEntity;
import ru.nsu.fit.krpo.mgbackend.exception.InvalidParametersException;
import ru.nsu.fit.krpo.mgbackend.service.ImageService;
import ru.nsu.fit.krpo.mgbackend.utils.ImageStatus;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/images")
@RequiredArgsConstructor
public class ImagesController {

	private final ImageService imageService;

	@PostMapping("upload")
	public ImageCreatedDto uploadImage(@ModelAttribute ImageUploadDto imageDto) {
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		long id = imageService.uploadImage(imageDto.getImage(), login);
		ImageCreatedDto imageCreatedDto = new ImageCreatedDto();
		imageCreatedDto.setImageId(id);
		return imageCreatedDto;
	}

	@GetMapping(value = "loadById/{imageId}")
	@ResponseBody
	public ImageEntity downloadImage(@PathVariable Long imageId) {
		ImageEntity entity = imageService.downloadImage(imageId);
		if (entity.getStatus() == ImageStatus.INVALID)
		{
			throw new InvalidParametersException("Image " + imageId + " is invalid");
		}
		return entity;
	}

	@GetMapping("loadByAuthor/{authorId}")
	@ResponseBody
	public AuthorsImagesDto downloadImagesByAuthor(@PathVariable Long authorId)  {
		List<Long> images = imageService.loadValidImageIdsByAuthor(authorId);
		AuthorsImagesDto imagesDto = new AuthorsImagesDto();
		imagesDto.setImageIds(images);

		return imagesDto;
	}
}
