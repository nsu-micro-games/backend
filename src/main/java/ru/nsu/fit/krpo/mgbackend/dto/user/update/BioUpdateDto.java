package ru.nsu.fit.krpo.mgbackend.dto.user.update;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BioUpdateDto {
	String newBio;
}
