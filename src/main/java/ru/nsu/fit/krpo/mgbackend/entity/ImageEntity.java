package ru.nsu.fit.krpo.mgbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Type;
import ru.nsu.fit.krpo.mgbackend.utils.ImageStatus;

import javax.persistence.*;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "images")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ImageEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	private Long owner;

	@Lob
	@Type(type = "org.hibernate.type.ImageType")
	private byte[] data;

	@Enumerated(EnumType.ORDINAL)
	@JsonIgnore
	private ImageStatus status;
}
