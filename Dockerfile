FROM openjdk:16-jdk-alpine
EXPOSE 8080

COPY build/libs/mg-backend-0.0.1-SNAPSHOT.jar mg-backend-0.0.1.jar
ENTRYPOINT ["java","-jar","/mg-backend-0.0.1.jar"]